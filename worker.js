const { parentPort } = require('worker_threads');
const axios = require('axios');

parentPort.on('message', async (data) => {
    console.log(`=========================`);

    const row = data.row;
    const id = data.offset;
    const provider = data.provider;
    const eventid = data.eventid;
    try {
        const betname = row.input_feed_BK1_bet;
        const bettype = row.input_feed_BK1_bet_type;
        const league = JSON.parse(row.output_feed_string).BK1_league;
        const cfs = JSON.parse(row.input_feed_string).valuing_data;

        let sport = JSON.parse(row.output_feed_string).sport;

        const data = {
            event_id: eventid,
            team_1: 'Team A',
            team_2: 'Team B',
            sport: sport,
            bets: [{ bet_type: bettype, bet_name: betname }]
        };

        console.log('Worker run');

        const response = await axios.post('http://localhost:3333/bets', data);
        let result;
        let score;
        console.log(sport);

        if (row.input_feed_BK1_game.includes('(') && row.input_feed_BK1_game.includes('Esports')) {
            if (sport == "soccer") {
                sport = "esports.soccer";
            }
            if (sport == "basketball") {
                sport = "esports.basketball";
            }
        }
        if (response.data.result) {
            result = JSON.stringify(response.data.result[0]);
            score = JSON.stringify(response.data.result[1]);
        } else {
            result = null;
            score = null;
        }
        console.log(result + score);

        const values = [
            id, row.uuid, row.timestamp, row.input_feed_fork_id, row.input_feed_BK1_name,
            sport, row.input_feed_BK1_game, row.input_feed_BK1_bet, row.input_feed_BK1_bet_type,
            row.input_feed_BK1_cf, score, response.data.event_id,
            result, row.output_feed_income, cfs, provider, league
        ];
        console.log("values");
        parentPort.postMessage({ done: true, data: values });
        console.log(`=========================`);
    } catch (error) {
        console.log('Error in worker:', error);
        parentPort.postMessage('error');
    }
});