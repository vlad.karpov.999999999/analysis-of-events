const axios = require('axios');


async function startProc(fork, serchDataBetweenTwoHours, event_id) {
    var dataBetsApi = await modifyDataBetweenTwoHours(serchDataBetweenTwoHours);
    var dataSerachfeed = await modifyDataForkBurger(fork, event_id);
    console.log("===============");
    console.log(dataSerachfeed);
    console.log("===============");
    var matchData = await sendMatchData(dataBetsApi, [dataSerachfeed]);
    return matchData;
}



async function modifyDataBetweenTwoHours(serchDataBetweenTwoHours) {
    var result = [];
    //console.log(serchDataBetweenTwoHours);
    for (let i = 0; i < serchDataBetweenTwoHours.length; i++) {
        fork = serchDataBetweenTwoHours[i];
        var eventid = JSON.parse(JSON.parse(fork.input_feed_string).BK1_event_meta).fi;
        var oneResult = await modifyDataForkBurger(fork, parseFloat(eventid));
        result.push(oneResult);
    }
    return result;
}

async function modifyDataForkBurger(fork, event_id) {

    var is_cyber = 0;

    //console.log(fork);

    if (fork.input_feed_BK1_game.includes('(') && fork.input_feed_BK1_game.includes('Esports')) {
        is_cyber = 1;
    }
    var teamsName = fork.input_feed_BK1_game;
    const teams = teamsName.split(" vs ");
    const team1 = teams[0];  // "Sporting Kansas City"
    const team2 = teams[1];

    return {
        bk_event_id: event_id,
        sport: JSON.parse(fork.output_feed_string).sport,
        team1: team1,
        team2: team2,
        bk_name: fork.input_feed_BK1_name,
        is_cyber: is_cyber,
        event_name: teamsName,
        league_name: JSON.parse(fork.output_feed_string).BK1_league
    }
    /*[{
            "bk_event_id": "143999982",
            "sport": "soccer",
            "team1": "FK Jablonec",
            "team2": "Slovacko",
            "bk_name": "bet365",
            "is_cyber": 0,
            "event_name": "FK Jablonec vs Slovacko",
            "league_name": "Czech Republic First League"
        },]
        */
}



async function sendMatchData(dataBetsApi, dataSerachfeed) {
    if (dataBetsApi == []) {
        return "Data not found";
    }

    const url = 'http://77.50.186.230:8087/match_data';
    const data = {
        source1_id: 17,
        source2_id: 16,
        source1_data: dataBetsApi,
        source2_data: dataSerachfeed,
        parameters: {
            types: {},
            REMOVE_CANDIDATE_THRESHOLD: 50,
            CONFIDENT_THRESHOLD: 99,
            MINIMAL_SIM_THRESHOLD: 50,
            FINAL_SIM_RATIO: 0.90
        }
    };
    const config = {
        headers: {
            'Content-Type': 'application/json',
        },
    };

    try {
        const response = await axios.post(url, data, config);
        //console.log('Data sent successfully:', response.data);
        return response.data;
    } catch (error) {
        console.error('Error sending data:', error.message);
    }
}


module.exports.modifyDataForkBurger = modifyDataForkBurger;
module.exports.startProc = startProc;