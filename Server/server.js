const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const { Pool } = require('pg');
const { calc_fulltime_win } = require('./calc_function'); // Импорт функции
const { calc_half_01_totals } = require('./calc_function'); // Импорт функции
const { calc_handicap_HalfAndFull_P1_P2 } = require('./handicap'); // Импорт функции
const { calc_totals_HalfAndFull_P1_P2 } = require('./total'); // Импорт функции
const { Client } = require('pg');
const fs = require('fs');


const sportScore = require('./sportScore');
const sportscore = new sportScore;

const pool = new Pool({
  host: '77.50.153.38',
  port: 5432,
  user: 'postgres',
  password: 'ArsTbarthRob2739*',
  database: 'logs'
});

const SportService = require('./helperclass');
var sportService = new SportService;

async function writeCurrentOffset(offset) {
  try {
    fs.appendFileSync('eventNoSportId.txt', offset.toString() + '\n');
  } catch (err) {
    console.error('Ошибка при записи в файл:', err);
  }
}


const app = express();
const port = 3333;

let eventData = {}; // Переменная для хранения данных с API

app.use(bodyParser.json());

app.post('/bets', async (req, res) => {
  const { event_id, team_1, team_2, sport, bets, scoreClient } = req.body;
  if (!event_id || !team_1 || !team_2 || !sport || !Array.isArray(bets)) {
    //console.log(event_id + " " + team_1 + " " + team_2 + " " + team_2 + " " + sport + " " + bets);
    return res.status(400).json({ error: 'Недостаточно данных' });

  }
  var result = await start(event_id, team_1, team_2, sport, bets, scoreClient);
  res.send(result);
});

startbd();
var client;
async function startbd() {
  client = await pool.connect();

  client.on('error', (err) => {
    console.error('Unexpected error on idle client', err);
    sendTelegramMessage(`Произошла ошибка: ${err}`);
    process.exit(-1);
  });
}

async function start(event_id, team_1, team_2, sport, bets, scoreClient) {

  eventData = await checkAndInsert(event_id);
  var sport_api;
  try {
    sport_api = sportService.getSportNameById(parseFloat(eventData.sport_id)).toLowerCase();
  } catch {
    writeCurrentOffset(eventData);
    return ["no sport", { scoreGame: "no sport" }];
  }
  //console.log(eventData + " " + sport);
  if (!sport_api && sport_api != sport) {

    if (sport_api && sport_api == sport) {

    } else {
      return "спорт не указан";
    }
  }
  var result = getBetTypeAndName(eventData, bets, sport.toLowerCase(), scoreClient);


  return {
    event_id: event_id,
    //team_1: eventData.home.name,
    //team_2: eventData.away.name,
    sport: sport,
    result: result
  };
}

function getBetTypeAndName(eventData, bets, sport, scoreClient) {
  var result;
  for (const bet of bets) {
    const { bet_type, bet_name } = bet;
    if (!bet_type || !bet_name) {
      continue;
    }
    result = globalCalc(bet_type, bet_name, eventData, sport, scoreClient);

  }
  return result;
}


function globalCalc(bet_type, bet_name, eventData, sport, scoreClient) {
  var score;
  ///=====================
  /// Выбор счета в зависимости от того какой период указан в bet_type
  ///=====================
  if (!scoreClient) {
    score = sportscore.errorFullLife(eventData, bet_name, bet_type, sport);
  } else {
    score = scoreClient;
  }

  if (score) {
    ///=====================
    /// Выбор счета в зависимости от того какой период указан в bet_type
    ///===================== 

    let result;

    if (bet_type === 'WIN' || bet_type === 'HALF_WIN' || bet_type === 'HALF_WIN_CORNERS' || bet_type === 'SET_WIN' || bet_type === 'WIN_OT' || bet_type === 'WIN_CORNERS' || bet_type === 'SET_WIN_OT') {
      console.log(bet_type + ' ' + sport + ' ' + score + ' ' + bet_name);
      result = calc_fulltime_win(sport, score, bet_name);
    }

    if (bet_type === 'HALF_TOTALS' || bet_type === 'TOTALS' || bet_type === 'ASIAN_TOTALS' || bet_type === 'HALF_ASIAN_TOTALS' || bet_type === 'TOTALS_CORNERS' || bet_type === 'HALF_TOTALS_CORNERS' || bet_type === 'SET_TOTALS') {
      console.log(bet_type + ' ' + sport + ' ' + score + ' ' + bet_name);
      result = calc_half_01_totals(score, bet_name);
    }

    if (bet_type === 'TEAM_TOTALS' || bet_type === 'HALF_TEAM_TOTALS' || bet_type === 'SET_TEAM_TOTALS') {
      console.log(bet_type + ' ' + sport + ' ' + score + ' ' + bet_name);
      result = calc_totals_HalfAndFull_P1_P2(sport, score, bet_name);
    }

    if (bet_type === 'HALF_HANDICAP' || bet_type === 'HANDICAP' || bet_type === 'CORNERS_HANDICAP' || bet_type === 'SET_HANDICAP') {
      console.log(bet_type + ' ' + sport + ' ' + score + ' ' + bet_name);
      result = calc_handicap_HalfAndFull_P1_P2(sport, score, bet_name);
    }
    if (bet_type.includes('CORNERS')) {
      return [result, { scoreCorners: score }];
    }

    return [result, { scoreGame: score }];
  } else {
    return ["no score", { scoreGame: "no score" }];
  }
}



///=====================
/// Функция для проверки наличия event_id в базе данных
///=====================
async function checkAndInsert(event_id) {
  let retries = 10000; // Количество попыток
  var result;

  while (retries) {
    try {
      // Проверка наличия записи с таким event_id
      const checkQuery = 'SELECT * FROM betsapi WHERE event_id = $1';
      const checkRes = await client.query(checkQuery, [event_id]);
      let eventData;

      if (checkRes.rows.length === 0) {
        try {
          const response = await axios.get(`https://api.b365api.com/v1/bet365/result?token=37142-BCVFD8myX0y8fk&event_id=${event_id}`);
          eventData = response.data.results[0];
        } catch (error) {
          console.error(`Ошибка при получении данных с API: ${error}`);
          sendTelegramMessage(`Ошибка при получении данных с API: ${error}`);
        }

        var sport
        try {
          sport = sportService.getSportNameById(parseFloat(eventData.sport_id)).toLowerCase();
        } catch {
          sport = null;
        }
        try {
          const insertQuery = 'INSERT INTO betsapi (bk_name, sport, event_id, data, home, away) VALUES ($1, $2, $3, $4, $5, $6)';
          await client.query(insertQuery, ["bet365", sport, event_id, JSON.stringify(eventData), eventData.home.name, eventData.away.name]);
        } catch {
          return null;
        }
        console.log(`Inserted new record with event_id: ${event_id}`);
        result = eventData;
      } else {
        const existingJsonData = checkRes.rows[0].data;
        console.log(`Record with event_id: ${event_id} already exists`);
        result = existingJsonData;
        break;
      }
    } catch (err) {
      onsole.error('Error:', err.stack);
      retries -= 1;
      console.log(`Осталось попыток: ${retries}`);
      await new Promise(resolve => setTimeout(resolve, 1000));
    }
  }

  return result;
}

async function sendTelegramMessage(text_msg) {
  const baseUrl = 'https://api.telegram.org/bot6644601768:AAFxsbDF2UKGPgiApMwK0EvDfU-etQd2GCI/sendMessage';
  const chatId = '-1001907647173';
  const replyToMessageId = '8';

  try {
    const response = await axios.get(baseUrl, {
      params: {
        chat_id: chatId,
        text: text_msg,
        reply_to_message_id: replyToMessageId
      }
    });
    console.log('Сообщение успешно отправлено:', response.data);
  } catch (error) {
    console.error('Ошибка при отправке сообщения:', error);
  }
}

app.listen(port, () => {
  console.log(`Сервис получения результата ставки запущен на порту ${port}`);
});
