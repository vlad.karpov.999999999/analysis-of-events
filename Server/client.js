const axios = require('axios');

const data = {
  event_id: 142997963,
  team_1: 'Team A',
  team_2: 'Team B',
  sport: 'soccer',
  bets: [
    { bet_type: 'HALF_HANDICAP', bet_name: 'HALF_01__HANDICAP__P2(0.75)' }
  ],
  scoreClient: '0:1'
};


// Отправляем POST-запрос на сервер
axios.post('http://localhost:3333/bets', data)
  .then((response) => {
    // Выводим полученные данные в консоль
    console.log('Получен ответ от сервера:');
    console.log(response.data);
    console.log(data.bets);
  })
  .catch((error) => {

    console.error(`Произошла ошибка: ${error}`);

  });


