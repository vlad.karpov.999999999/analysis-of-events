const axios = require('axios');

// Формируем данные для отправки
const data = {
    //https://api.b365api.com/v1/bet365/result?token=167732-E4fQJNGIglYe7u&event_id=143723906
    event_id: 143723906,
    team_1: 'Team A',
    team_2: 'Team B',
    sport: 'Soccer',
    bets: [
      { bet_type: 'FULLTIME_WIN', bet_name: 'WIN__X2' },
      { bet_type: 'FULLTIME_WIN', bet_name: 'WIN__X1' },
      { bet_type: 'FULLTIME_WIN', bet_name: 'WIN__PX' },
      { bet_type: 'FULLTIME_WIN', bet_name: 'WIN__P2' },
      { bet_type: 'FULLTIME_WIN', bet_name: 'WIN__P1' },
      { bet_type: 'FULLTIME_WIN', bet_name: 'WIN__1X' },
      { bet_type: 'FULLTIME_WIN', bet_name: 'WIN__12' },


      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(7.75)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(7.5)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(7.25)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(7.0)'},

      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(6.75)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(6.5)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(6.25)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(6.0)'},

      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(5.75)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(5.5)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(5.25)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(5.0)'},

      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(4.75)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(4.5)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(4.25)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(4.0)'},

      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(3.75)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(3.5)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(3.25)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(3.0)'},

      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(2.75)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(2.5)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(2.25)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(2.0)'},

      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(1.75)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(1.5)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(1.25)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(1.0)'},

      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(0.75)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__UNDER(0.5)'},


      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(7.75)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(7.5)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(7.25)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(7.0)'},

      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(6.75)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(6.5)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(6.25)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(6.0)'},

      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(5.75)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(5.5)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(5.25)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(5.0)'},
 
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(4.75)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(4.5)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(4.25)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(4.0)'},

      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(3.75)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(3.5)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(3.25)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(3.0)'},

      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(2.75)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(2.5)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(2.25)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(2.0)'},

      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(1.75)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(1.5)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(1.25)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(1.0)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(0.75)'},
      { bet_type: 'HALF_01__TOTALS', bet_name: 'HALF_01__TOTALS__OVER(0.5)'},

      //VLVVVLADLADLALD
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__OVER(0.25)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__OVER(0.5)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__OVER(0.75)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__OVER(1)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__OVER(1.25)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__OVER((1.5)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__OVER(1.75)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__OVER(2)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__OVER(2.25)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__OVER(2.5)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__OVER(2.75)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__OVER(3)'},


      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__UNDER(0.25)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__UNDER(0.5)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__UNDER(0.75)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__UNDER(1)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__UNDER(1.25)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__UNDER((1.5)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__UNDER(1.75)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__UNDER(2)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__UNDER(2.25)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__UNDER(2.5)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__UNDER(2.75)'},
      { bet_type: 'HALF_01__P1__TOTALS', bet_name: 'HALF_01__P1__TOTALS__UNDER(3)'},


      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__OVER(0.25)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__OVER(0.5)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__OVER(0.75)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__OVER(1)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__OVER(1.25)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__OVER((1.5)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__OVER(1.75)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__OVER(2)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__OVER(2.25)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__OVER(2.5)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__OVER(2.75)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__OVER(3)'},


      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__UNDER(0.25)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__UNDER(0.5)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__UNDER(0.75)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__UNDER(1)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__UNDER(1.25)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__UNDER((1.5)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__UNDER(1.75)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__UNDER(2)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__UNDER(2.25)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__UNDER(2.5)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__UNDER(2.75)'},
      { bet_type: 'HALF_01__P2__TOTALS', bet_name: 'HALF_01__P2__TOTALS__UNDER(3)'},


      

      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__OVER(0.25)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__OVER(0.5)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__OVER(0.75)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__OVER(1)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__OVER(1.25)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__OVER((1.5)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__OVER(1.75)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__OVER(2)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__OVER(2.25)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__OVER(2.5)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__OVER(2.75)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__OVER(3)'},


      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__UNDER(0.25)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__UNDER(0.5)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__UNDER(0.75)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__UNDER(1)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__UNDER(1.25)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__UNDER((1.5)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__UNDER(1.75)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__UNDER(2)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__UNDER(2.25)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__UNDER(2.5)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__UNDER(2.75)'},
      { bet_type: 'HALF_02__P1__TOTALS', bet_name: 'HALF_02__P1__TOTALS__UNDER(3)'},


      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__OVER(0.25)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__OVER(0.5)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__OVER(0.75)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__OVER(1)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__OVER(1.25)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__OVER((1.5)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__OVER(1.75)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__OVER(2)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__OVER(2.25)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__OVER(2.5)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__OVER(2.75)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__OVER(3)'},


      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__UNDER(0.25)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__UNDER(0.5)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__UNDER(0.75)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__UNDER(1)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__UNDER(1.25)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__UNDER((1.5)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__UNDER(1.75)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__UNDER(2)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__UNDER(2.25)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__UNDER(2.5)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__UNDER(2.75)'},
      { bet_type: 'HALF_02__P2__TOTALS', bet_name: 'HALF_02__P2__TOTALS__UNDER(3)'},



      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__OVER(0.25)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__OVER(0.5)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__OVER(0.75)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__OVER(1)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__OVER(1.25)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__OVER((1.5)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__OVER(1.75)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__OVER(2)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__OVER(2.25)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__OVER(2.5)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__OVER(2.75)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__OVER(3)'},


      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__UNDER(0.25)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__UNDER(0.5)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__UNDER(0.75)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__UNDER(1)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__UNDER(1.25)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__UNDER((1.5)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__UNDER(1.75)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__UNDER(2)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__UNDER(2.25)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__UNDER(2.5)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__UNDER(2.75)'},
      { bet_type: 'P1__TOTALS', bet_name: 'P1__TOTALS__UNDER(3)'},


      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__OVER(0.25)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__OVER(0.5)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__OVER(0.75)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__OVER(1)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__OVER(1.25)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__OVER((1.5)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__OVER(1.75)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__OVER(2)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__OVER(2.25)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__OVER(2.5)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__OVER(2.75)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__OVER(3)'},


      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__UNDER(0.25)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__UNDER(0.5)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__UNDER(0.75)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__UNDER(1)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__UNDER(1.25)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__UNDER((1.5)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__UNDER(1.75)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__UNDER(2)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__UNDER(2.25)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__UNDER(2.5)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__UNDER(2.75)'},
      { bet_type: 'P2__TOTALS', bet_name: 'P2__TOTALS__UNDER(3)'},




      { bet_type: 'HALF_01__HANDICAP', bet_name: 'HALF_01__HANDICAP__P1(-1)'},
      { bet_type: 'HALF_01__HANDICAP', bet_name: 'HALF_01__HANDICAP__P1(-0.75)'},
      { bet_type: 'HALF_01__HANDICAP', bet_name: 'HALF_01__HANDICAP__P1(-0.5)'},
      { bet_type: 'HALF_01__HANDICAP', bet_name: 'HALF_01__HANDICAP__P1(-0.25)'},
      { bet_type: 'HALF_01__HANDICAP', bet_name: 'HALF_01__HANDICAP__P1(0.25)'},
      { bet_type: 'HALF_01__HANDICAP', bet_name: 'HALF_01__HANDICAP__P1(0.5)'},
      { bet_type: 'HALF_01__HANDICAP', bet_name: 'HALF_01__HANDICAP__P1(0.75)'},
      { bet_type: 'HALF_01__HANDICAP', bet_name: 'HALF_01__HANDICAP__P1(1)'},


      { bet_type: 'HALF_01__HANDICAP', bet_name: 'HALF_01__HANDICAP__P2(-1)'},
      { bet_type: 'HALF_01__HANDICAP', bet_name: 'HALF_01__HANDICAP__P2(-0.75)'},
      { bet_type: 'HALF_01__HANDICAP', bet_name: 'HALF_01__HANDICAP__P2(-0.5)'},
      { bet_type: 'HALF_01__HANDICAP', bet_name: 'HALF_01__HANDICAP__P2(-0.25)'},
      { bet_type: 'HALF_01__HANDICAP', bet_name: 'HALF_01__HANDICAP__P2(0.25)'},
      { bet_type: 'HALF_01__HANDICAP', bet_name: 'HALF_01__HANDICAP__P2(0.5)'},
      { bet_type: 'HALF_01__HANDICAP', bet_name: 'HALF_01__HANDICAP__P2(0.75)'},
      { bet_type: 'HALF_01__HANDICAP', bet_name: 'HALF_01__HANDICAP__P2(1)'},


      { bet_type: 'HALF_02__HANDICAP', bet_name: 'HALF_02__HANDICAP__P1(-1)'},
      { bet_type: 'HALF_02__HANDICAP', bet_name: 'HALF_02__HANDICAP__P1(-0.75)'},
      { bet_type: 'HALF_02__HANDICAP', bet_name: 'HALF_02__HANDICAP__P1(-0.5)'},
      { bet_type: 'HALF_02__HANDICAP', bet_name: 'HALF_02__HANDICAP__P1(-0.25)'},
      { bet_type: 'HALF_02__HANDICAP', bet_name: 'HALF_02__HANDICAP__P1(0.25)'},
      { bet_type: 'HALF_02__HANDICAP', bet_name: 'HALF_02__HANDICAP__P1(0.5)'},
      { bet_type: 'HALF_02__HANDICAP', bet_name: 'HALF_02__HANDICAP__P1(0.75)'},
      { bet_type: 'HALF_02__HANDICAP', bet_name: 'HALF_02__HANDICAP__P1(1)'},


      { bet_type: 'HALF_02__HANDICAP', bet_name: 'HALF_02__HANDICAP__P2(-1)'},
      { bet_type: 'HALF_02__HANDICAP', bet_name: 'HALF_02__HANDICAP__P2(-0.75)'},
      { bet_type: 'HALF_02__HANDICAP', bet_name: 'HALF_02__HANDICAP__P2(-0.5)'},
      { bet_type: 'HALF_02__HANDICAP', bet_name: 'HALF_02__HANDICAP__P2(-0.25)'},
      { bet_type: 'HALF_02__HANDICAP', bet_name: 'HALF_02__HANDICAP__P2(0.25)'},
      { bet_type: 'HALF_02__HANDICAP', bet_name: 'HALF_02__HANDICAP__P2(0.5)'},
      { bet_type: 'HALF_02__HANDICAP', bet_name: 'HALF_02__HANDICAP__P2(0.75)'},
      { bet_type: 'HALF_02__HANDICAP', bet_name: 'HALF_02__HANDICAP__P2(1)'},


      { bet_type: 'HANDICAP__P1', bet_name: 'HANDICAP__P1(-2)'},
      { bet_type: 'HANDICAP__P1', bet_name: 'HANDICAP__P1(-0.75)'},
      { bet_type: 'HANDICAP__P1', bet_name: 'HANDICAP__P1(-0.5)'},
      { bet_type: 'HANDICAP__P1', bet_name: 'HANDICAP__P1(-0.25)'},
      { bet_type: 'HANDICAP__P1', bet_name: 'HANDICAP__P1(0.25)'},
      { bet_type: 'HANDICAP__P1', bet_name: 'HANDICAP__P1(0.5)'},
      { bet_type: 'HANDICAP__P1', bet_name: 'HANDICAP__P1(0.75)'},
      { bet_type: 'HANDICAP__P1', bet_name: 'HANDICAP__P1(1)'},


      { bet_type: 'HANDICAP__P2', bet_name: 'HANDICAP__P2(-3)'},
      { bet_type: 'HANDICAP__P2', bet_name: 'HANDICAP__P2(-0.75)'},
      { bet_type: 'HANDICAP__P2', bet_name: 'HANDICAP__P2(-0.5)'},
      { bet_type: 'HANDICAP__P2', bet_name: 'HANDICAP__P2(-0.25)'},
      { bet_type: 'HANDICAP__P2', bet_name: 'HANDICAP__P2(0.25)'},
      { bet_type: 'HANDICAP__P2', bet_name: 'HANDICAP__P2(0.5)'},
      { bet_type: 'HANDICAP__P2', bet_name: 'HANDICAP__P2(0.75)'},
      { bet_type: 'HANDICAP__P2', bet_name: 'HANDICAP__P2(1)'},
    ]
  };
  

// Отправляем POST-запрос на сервер
axios.post('http://localhost:3333/bets', data)
  .then((response) => {
    // Выводим полученные данные в консоль
    console.log('Получен ответ от сервера:');
    console.log(response.data);
  })
  .catch((error) => {
    // Обработка ошибок
    console.error(`Произошла ошибка: ${error}`);
  });
