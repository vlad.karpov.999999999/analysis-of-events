function calc_fulltime_win(sport, score, bet_name) {
    var homeScore;
    var awayScore;
    try {
        [homeScore, awayScore] = score.split(':').map(Number);
    } catch {
        return null;
    }
    let result = 0;
    const position = bet_name.indexOf("WIN");
    let name = bet_name.substring(position);
    console.log(name);
    switch (name) {
        case 'WIN__X2':
            result = awayScore >= homeScore ? 1 : -1;
            break;
        case 'WIN__PX':
            result = homeScore === awayScore ? 1 : -1;
            break;
        case 'WIN__P2':
            result = awayScore > homeScore ? 1 : -1;
            break;
        case 'WIN__P1':
            result = homeScore > awayScore ? 1 : -1;
            break;
        case 'WIN__1X':
            result = homeScore >= awayScore ? 1 : -1;
            break;
        case 'WIN__X1':
            result = homeScore >= awayScore ? 1 : -1;
            break;
        case 'WIN__12':
            result = homeScore !== awayScore ? 1 : -1;
            break;
        case 'WIN_OT__P1':
            if (homeScore > awayScore) {
                result = 1;
            } else if (homeScore == awayScore) {
                result = 0;
            } else {
                result = -1;
            }
            break;
        case 'WIN_OT__P2':
            if (awayScore > homeScore) {
                result = 1;
            } else if (homeScore == awayScore) {
                result = 0;
            } else {
                result = -1;
            }
            break;
        default:
            result = 0; // Неизвестный тип ставки в победе во всей игре
    }
    return result;
}




function calc_half_01_totals(score, bet_name) {
    let result;
    let homeScore;
    let awayScore;
    if (score) {
        [homeScore, awayScore] = score.split(':').map(Number);
    } else {
        return null;
    }

    const firstHalfGoals = homeScore + awayScore;
    const betParts = bet_name.split('(');
    const betType = betParts[0];
    const betValue = parseFloat(betParts[1].slice(0, -1));

    if (betType.includes('OVER')) {
        console.log(firstHalfGoals - betValue);

        if (betValue % 1 === 0.25) {
            if (firstHalfGoals - betValue === 0.25) {
                result = 0.5;
            } else if (firstHalfGoals - betValue < -0.25) {
                result = -1;
            } else if (firstHalfGoals - betValue === -0.25) {
                result = -0.5;
            } else {
                result = 1;
            }
        }

        if (betValue % 1 === 0.5) {
            if (firstHalfGoals > betValue) {
                result = 1;
            } else {
                result = -1;
            }
        }

        if (betValue % 1 === 0.75) {
            if (firstHalfGoals - betValue === 0.25) {
                result = 0.5;
            } else if (firstHalfGoals < betValue) {
                result = -1;
            } else {
                result = 1;
            }
        }

        if (betValue % 1 === 0) {
            if (firstHalfGoals > betValue) {
                result = 1;
            } else if (firstHalfGoals === betValue) {
                result = 0;
            } else if (firstHalfGoals < betValue) {
                result = -1;
            }
        }

    } else if (betType.includes('UNDER')) {
        console.log(betValue - firstHalfGoals);
        if (betValue % 1 === 0.25) {
            if (firstHalfGoals - betValue === 0.25) {
                result = -0.5;
            } else if (firstHalfGoals - betValue < -0.25) {
                result = 1;
            } else if (firstHalfGoals - betValue === -0.25) {
                result = 0.5;
            } else {
                result = -1;
            }
        }

        if (betValue % 1 === 0.50) {
            if (firstHalfGoals < betValue) {
                result = 1;
            } else {
                result = -1;
            }
        }

        if (betValue % 1 === 0.75) {
            if (firstHalfGoals - betValue === 0.25) {
                result = -0.5;
            } else if (firstHalfGoals > betValue) {
                result = -1;
            } else {
                result = 1;
            }
        }

        if (betValue % 1 === 0) {
            if (firstHalfGoals < betValue) {
                result = 1;
            } else if (firstHalfGoals === betValue) {
                result = 0;
            } else if (firstHalfGoals > betValue) {
                result = -1;
            }
        }
    } else {
        result = 'error_bet_type';
    }
    return result;
}


module.exports = {
    calc_fulltime_win,
    calc_half_01_totals
};
