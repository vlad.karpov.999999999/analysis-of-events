///=====================
/// Функция для расчета исхода ставки на основе типа ставки, общего значения и счета
///=====================
function calculateBetOutcome(betType, totalValue, score) {
  const [team1Goals, team2Goals] = score.split(':').map(Number);
  let outcome = '';
  
  ///=====================
  /// Расчет для команды P1
  ///=====================
  if (betType.includes('P1')) {
    if (betType.includes('OVER')) {
      if (team1Goals > totalValue) {
        outcome = 1;
      } else if (team1Goals < totalValue) {
        outcome = -1;
      } else if (team1Goals == totalValue) {
        outcome = 0;
      } else {
        outcome = "";
      }
    } else if (betType.includes('UNDER')) {
      if (team1Goals < totalValue) {
        outcome = 1;
      } else if (team1Goals > totalValue) {
        outcome = -1;
      } else if (team1Goals == totalValue) {
        outcome = 0;
      } else {
        outcome = "";
      }
    }
    ///=====================
    /// Расчет для команды P2
    ///=====================
  } else if (betType.includes('P2')) {
    if (betType.includes('OVER')) {
      if (team2Goals > totalValue) {
        outcome = 1;
      } else if (team2Goals < totalValue) {
        outcome = -1;
      } else if (team2Goals == totalValue) {
        outcome = 0;
      } else {
        outcome = "";
      }
    } else if (betType.includes('UNDER')) {
      if (team2Goals < totalValue) {
        outcome = 1;
      } else if (team2Goals > totalValue) {
        outcome = -1;
      } else if (team2Goals == totalValue) {
        outcome = 0;
      } else {
        outcome = "";
      }
    }
  }

  return outcome;
}

///=====================
/// Функция для получения исхода
///=====================
function evaluateBet(bet, score) {
  const totalMatch = bet.match(/\(([^)]+)\)/);
  if (!totalMatch) {
    return "Invalid bet format";
  }

  let totalValue = parseFloat(totalMatch[1]);
  let result;

 
  ///=====================
  /// Проверка на целое число
  ///=====================
  if (Number.isInteger(totalValue)) {
    result = calculateBetOutcome(bet, totalValue, score);
  } else {
    const fraction = totalValue % 1;
    ///=====================
    /// Расчет для дробных чисел с десятичной частью 0.25 или 0.75
    ///=====================
    if (fraction === 0.25) {

      const part1 = calculateBetOutcome(bet, Math.floor(totalValue), score);
      const part2 = calculateBetOutcome(bet, Math.floor(totalValue) + 0.5, score);
      console.log(part1 + " " + part2);
      result = (part1 + part2) / 2;

    } else if (fraction === 0.5) {
      
      const part1 = calculateBetOutcome(bet, totalValue, score);
      const part2 = calculateBetOutcome(bet, totalValue, score);
      result = (part1 + part2) / 2;

    } else if (fraction === 0.75) {

      const part1 = calculateBetOutcome(bet, totalValue - 0.25, score);
      const part2 = calculateBetOutcome(bet, totalValue + 0.25, score);
      result = (part1 + part2) / 2;

    } else {
      result = 0; // Или какое-либо другое значение по умолчанию
    }
  }
  return result;
}

///=====================
/// Основная функция для расчета исхода ставки
///=====================

function calc_totals_HalfAndFull_P1_P2(sport, score, bet) {
  var outcome;
  //if (sport == "soccer" || sport == "basketball") {
    outcome = evaluateBet(bet, score);
  //}
  return outcome;
}

module.exports = {
  calc_totals_HalfAndFull_P1_P2
}
