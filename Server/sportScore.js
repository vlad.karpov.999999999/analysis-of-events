class sportScore {

    constructor() {
        //this.run();
    }

    errorFullLife(eventData, bet_name, bet_type, sport) {
        var result;

        switch (sport) {
            case 'soccer': case 'esports.soccer':
                console.log(sport);
                var period = this.extractPeriodSoccer(bet_name);
                var score = this.getScoresSoccer(eventData, bet_type, period);
                //console.log(score);
                result = score;
                break;
            case 'basketball': case 'esports.basketball':
                var period = this.extractPeriodBasketball(bet_name);
                var score = this.getScoresBasketball(eventData, period);
                //console.log(score);
                result = score;
                break;
            case 'tennis':
                var period = this.extractPeriodTennis(bet_name, eventData);
                var score = this.getScoresTennis(eventData, period);
                //console.log(score);
                result = score;
                break;
            default:
                result = 0;
        }
        return result;
    }

    getScoresSoccer(eventData, bet_type, period) {
        try {
            if (!bet_type.includes('CORNERS')) {
                //console.log(eventData);
                var scores = eventData.scores;
                if (scores && period && scores[period]) {
                    return `${scores[period].home}:${scores[period].away}`;
                } else if (scores) {
                    const lastPeriod = Object.keys(scores).pop();
                    return `${scores[lastPeriod].home}:${scores[lastPeriod].away}`;
                }
            } else {
                const cornersScore = [eventData.stats.corner_h, eventData.stats.corner_f];
                if (period && cornersScore[period - 1]) {
                    return `${cornersScore[period - 1][0]}:${cornersScore[period - 1][1]}`;
                } else if (cornersScore.length > 0) {
                    const lastPeriod = cornersScore.length - 1;
                    return `${cornersScore[lastPeriod][0]}:${cornersScore[lastPeriod][1]}`;
                }
            }
        } catch {
            return null;
        }
    }

    getScoresBasketball(eventData, period) {
        try {
            const scores = eventData.scores;
            if (period === 'half') {
                return this.calculateHalfTimeScores(scores);
            } else if (typeof period === 'number' && scores && scores[period]) {
                if (period >= 3) {
                    period++;
                }
                return `${scores[period].home}:${scores[period].away}`;
            } else {
                const lastPeriod = Object.keys(scores).pop();
                return `${scores[lastPeriod].home}:${scores[lastPeriod].away}`;
            }
        } catch (error) {
            console.error(error);
            return null;
        }
    }

    getScoresTennis(eventData, period) {
        try {
            const scores = eventData.ss;
            const sets = scores.split(',');
            if (period < 1 || period > sets.length) {
                return null;
            }
            return sets[period - 1];
        } catch (error) {
            console.error(error);
            return null;
        }
    }

    calculateHalfTimeScores(scores) {
        let halfTimeHome = 0;
        let halfTimeAway = 0;
        if (scores) {
            if (scores['1']) {
                halfTimeHome += parseInt(scores['1'].home, 10);
                halfTimeAway += parseInt(scores['1'].away, 10);
            }
            if (scores['2']) {
                halfTimeHome += parseInt(scores['2'].home, 10);
                halfTimeAway += parseInt(scores['2'].away, 10);
            }
        }
        else {
            return null;
        }

        return `${halfTimeHome}:${halfTimeAway}`;
    }

    calculateScoresTennis(scores) {
        let halfTimeHome = 0;
        let halfTimeAway = 0;
        if (scores) {
            if (scores['1']) {
                halfTimeHome += parseInt(scores['1'].home, 10);
                halfTimeAway += parseInt(scores['1'].away, 10);
            }
            if (scores['2']) {
                halfTimeHome += parseInt(scores['2'].home, 10);
                halfTimeAway += parseInt(scores['2'].away, 10);
            }
            if (scores['3']) {
                halfTimeHome += parseInt(scores['3'].home, 10);
                halfTimeAway += parseInt(scores['3'].away, 10);
            }
        }
        else {
            return null;
        }
        return `${halfTimeHome}:${halfTimeAway}`;
    }

    ///=====================
    /// Функция для проверки наличия периода и извлечения его номера
    ///=====================

    extractPeriodTennis(bet_name, match_data) {

        if (bet_name.includes("GAME")) {
            const gameMatch = bet_name.match(/GAME__(\d+)_(\d+)__P(\d)/);
            if (!gameMatch) return null;

            const gameNumber = parseInt(gameMatch[1], 10);
            const setsScores = match_data.ss.split(",").map(set => set.split("-").map(Number));

            const setNumber = calculateSetNumber(gameNumber, setsScores);
            console.log(setNumber);
            return setNumber;

            const playerNumber = parseInt(gameMatch[3], 10);
            const winningTeam = findGameEvent(gameNumber, match_data);

            if (bet_name.includes(winningTeam)) {
                console.log("win");
                return 1;
            } else {
                console.log("gameover");
                return 0;
            };
        } else {
            //const match = bet_name.match(/HALF_(\d+)_/);
            const set = bet_name.match(/SET_(\d+)_/);
            return set ? parseInt(set[1], 10) : null;
        }
    }

    extractPeriodSoccer(bet_name) {
        const match = bet_name.match(/HALF_(\d+)_/);
        return match ? parseInt(match[1], 10) : null;
    }

    extractPeriodBasketball(bet_name) {
        const half = bet_name.match(/HALF_(\d+)_/);
        const set = bet_name.match(/SET_(\d+)_/);
        if (half && half[1] === '01') {
            return 'half';
        } else if (set) {
            return parseInt(set[1], 10);
        } else {
            return null;
        }
    }

    ///=====================
    /// Функция для проверки наличия периода и извлечения его номера
    ///=====================

}
///=====================
/// Теннис
///=====================

function calculateSetNumber(gameNumber, setsScores) {
    let totalGames = 0, setNumber = 1;
    for (let setScore of setsScores) {
        totalGames += setScore[0] + setScore[1];
        if (gameNumber <= totalGames) {
            break;
        }
        setNumber++;
    }
    return setNumber;
}

function findGameEvent(gameNumber, match_data) {
    console.log(`Looking for Game ${gameNumber}`);
    console.log(match_data.events);

    const gameEvent = match_data.events.find(event => {
        const gameMatch = event.text.match(new RegExp(`Game ${gameNumber}(?:[^0-9]|$)`));
        return gameMatch !== null;
    });

    if (!gameEvent) {
        console.log(`Game ${gameNumber} not found`);
        return null;
    }
    const playerName = extractPlayerName(gameEvent.text);



    console.log(`Found: ${gameEvent.text}`);

    const winner = findPlayerSide(playerName, match_data);
    console.log(`winner: ${winner}`);
    return winner;
}

function findPlayerSide(playerName, matchData) {
    const homePlayerName = matchData.home.name;
    const awayPlayerName = matchData.away.name;

    if (playerName === homePlayerName) {
        return 'P1';
    } else if (playerName === awayPlayerName) {
        return 'P2';
    } else {
        return null;
    }
}

function extractPlayerName(gameEventText) {
    const parts = gameEventText.split(" - "); // Разделяем строку по " - "
    if (parts.length > 1) {
        return parts[1]; // Возвращаем второй элемент массива, который является именем игрока
    }
    return null; // В случае, если формат строки не соответствует ожидаемому
}
///=====================
/// Теннис
///=====================

module.exports = sportScore;
