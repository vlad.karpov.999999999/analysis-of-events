///=====================
/// Функция для расчета исхода ставки
///=====================
function calculateBets(score, bet_name) {
    ///=====================
    /// Разбиваем счет на две части и преобразуем их в числа
    ///=====================
    const [team1Score, team2Score] = score.split(':').map(Number);

    ///=====================
    /// Получаем handicap из строки bet_name
    ///=====================
    var regex = /-?\d+(\.\d+)?(?=\))/;
    var match = bet_name.match(regex);
    console.log(bet_name + "=====");
    const number = parseFloat(match[0]);

    ///=====================
    /// Получаем команду (P1 или P2) из строки bet_name
    ///=====================
    regex = /P\d+/;
    match = bet_name.match(regex);

    const team = match[0];

    ///=====================
    /// Генерируем handicap на основе полученного числа
    ///=====================
    const handicap = generateHandicaps(number);

    //console.log(handicap);

    ///=====================
    /// Рассчитываем исход ставки в зависимости от типа handicap
    ///=====================
    var outcome = '';
    if (team == "P1") {
        outcome = handicap[1] === 0 ?
            calculateOutcome(handicap[0], team1Score, team2Score) :
            calculateSplitOutcome(handicap[0], handicap[1], team1Score, team2Score);
    }
    if (team == "P2") {
        outcome = handicap[1] === 0 ?
            calculateOutcome(handicap[0], team2Score, team1Score) :
            calculateSplitOutcome(handicap[0], handicap[1], team2Score, team1Score);
    }


    return outcome;
}

///=====================
/// Функция для генерации handicap на основе входного числа
///=====================
function generateHandicaps(input) {
    let output = '';
    const floorValue = Math.floor(Math.abs(input));
    const decimalPart = Math.abs(input) - floorValue;

    ///=====================
    /// Определяем тип handicap на основе десятичной части числа
    ///=====================
    if (decimalPart === 0) {
        output = [input, 0];
    } else if (decimalPart === 0.25) {
        output = [floorValue, floorValue + 0.5];
    } else if (decimalPart === 0.5) {
        output = [Math.abs(input), 0];
    } else if (decimalPart === 0.75) {
        output = [floorValue + 0.5, floorValue + 1];
    } else {
        output = [floorValue, floorValue + 0.5];
    }

    ///=====================
    /// Если число отрицательное, меняем знак у handicap
    ///=====================
    if (input < 0) {
        if (Number.isInteger(input) && input < 0) {
            output = output.map(x => x !== input ? x : x);
        } else {
            output = output.map(x => x !== 0 ? -x : x);
        }

    }
    //console.log(output);
    return output;
}

///=====================
/// Функция для расчета исхода ставки с разделенным handicap
///=====================
function calculateSplitOutcome(handicap1, handicap2, score1, score2) {
    ///=====================
    /// Словарь возможных исходов для комбинаций результатов
    ///=====================
    const outcomes = {
        "1": {
            "1": 1,
            "-1": "Недопустимый исход",
            "0": 0.5
        },
        "-1": {
            "1": "Недопустимый исход",
            "-1": -1,
            "0": -0.5
        },
        "0": {
            "1": 0.5,
            "-1": -0.5,
            "0": 0
        }
    };

    const outcome1 = calculateOutcome(handicap1, score1, score2);
    const outcome2 = calculateOutcome(handicap2, score1, score2);

    return outcomes[outcome1][outcome2];
}

///=====================
/// Функция для расчета исхода ставки с одиночным handicap
///=====================
function calculateOutcome(handicap, score1, score2) {
    console.log(handicap + ' ' + score1 + ' ' + score2);
    const newScore1 = score1 + handicap;
    if (newScore1 > score2) return "1";
    if (newScore1 < score2) return "-1";
    return "0";
}

///=====================
/// Основная функция для расчета ставок на основе счета и типа ставки
///=====================
function calc_handicap_HalfAndFull_P1_P2(sport, score, bet_name) {
    var result;
    //if (sport == "soccer" || sport == "basketball") {

    //}
    result = calculateBets(score, bet_name);

    return parseFloat(result);
}
///bet_name: 'HANDICAP__P2(0)',
//result: '[null,"0:0"]'
//// вернуть 0 
///
///{
//    bet_type: 'HANDICAP',
//    bet_name: 'HANDICAP__P1(-2)',
//   result: '[null,"3:2"]'
// } вернуть -1
module.exports = {
    calc_handicap_HalfAndFull_P1_P2
}
