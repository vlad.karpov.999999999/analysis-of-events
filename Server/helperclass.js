class SportService {
    constructor() {
        this.sports = [
            { "sport_id": 1, "Name": "Soccer" },
            { "sport_id": 13, "Name": "Tennis" },
            { "sport_id": 78, "Name": "Handball" },
            { "sport_id": 2, "Name": "Horse Racing" },
            { "sport_id": 17, "Name": "Ice Hockey" },
            { "sport_id": 12, "Name": "American Football" },
            { "sport_id": 83, "Name": "Futsal" },
            { "sport_id": 92, "Name": "Table Tennis" },
            { "sport_id": 8, "Name": "Rugby Union" },
            { "sport_id": 36, "Name": "Australian Rules" },
            { "sport_id": 9, "Name": "Boxing" },
            { "sport_id": 90, "Name": "Floorball" },
            { "sport_id": 110, "Name": "Water Polo" },
            { "sport_id": 151, "Name": "E-sports" },
            { "sport_id": 148, "Name": "Surfing" },
            { "sport_id": 18, "Name": "Basketball" },
            { "sport_id": 91, "Name": "Volleyball" },
            { "sport_id": 16, "Name": "Baseball" },
            { "sport_id": 4, "Name": "Greyhounds" },
            { "sport_id": 14, "Name": "Snooker" },
            { "sport_id": 3, "Name": "Cricket" },
            { "sport_id": 15, "Name": "Darts" },
            { "sport_id": 94, "Name": "Badminton" },
            { "sport_id": 19, "Name": "Rugby League" },
            { "sport_id": 66, "Name": "Bowls" },
            { "sport_id": 75, "Name": "Gaelic Sports" },
            { "sport_id": 95, "Name": "Beach Volleyball" },
            { "sport_id": 107, "Name": "Squash" },
            { "sport_id": 162, "Name": "MMA" }
        ];
    }

    getSportNameById(id) {
        const sport = this.sports.find(sport => sport.sport_id === id);
        return sport ? sport.Name : null;
    }
}

module.exports = SportService;