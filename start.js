const axios = require('axios');
const fs = require('fs');
const path = require('path');
const { BigQuery } = require('@google-cloud/bigquery');
const keyFilename = path.join(__dirname, 'tokyo-carving-401111-4a13a5ef21fa.json');

const bigquery = new BigQuery({
    keyFilename: keyFilename
});

// Идентификаторы событий, которые уже были добавлены
const processedEventIds = new Set();

async function insertOddsDataBigQuery(dataObj) {
    console.log('Insert run');
    const datasetId = '610attemptn2';
    const tableId = 'apiOdds';

    if (!dataObj) {
        console.error('No data to insert into BigQuery table.');
        return;
    }

    // Фильтрация объектов данных для вставки только новых событий
    const newEvents = dataObj.filter(event => !processedEventIds.has(event.bk_event_id));

    // Добавление новых bk_event_id в набор для последующей обработки
    newEvents.forEach(event => processedEventIds.add(event.bk_event_id));

    if (newEvents.length === 0) {
        console.log('No new events to insert.');
        return;
    }

    try {
        await bigquery.dataset(datasetId).table(tableId).insert(newEvents);
        console.log(`Inserted ${newEvents.length} new events into BigQuery table.`);
    } catch (error) {
        console.error('Error inserting data:', error);
        if (error.response && error.response.data) {
            console.error('Error details:', error.response.data);
        }
    }
}

async function gatherOdds() {
    console.log("===========gatherOdds==========");
    const url = "http://api.oddscp.com:8111/events?bk_name=bet365&token=2501efd9e9faf3de5a5d19414c302c18";

    try {
        const response = await axios.get(url);
        const events = response.data;
        const bigQueryData = events.map(item => ({
            bk_event_id: item.bk_event_id,
            meta: item.meta,
            bk_event_native_id: item.bk_event_native_id,
            direct_link: item.direct_link,
            sport: item.sport,
            event_name: item.event_name
        }));

        await insertOddsDataBigQuery(bigQueryData);
    } catch (error) {
        console.error('Ошибка при получении данных:', error);
    }
}

function startGatheringOdds() {
    console.log("===========START==========");
    gatherOdds();
    setInterval(gatherOdds, 30000); // Повторять каждые 30 секунд.
}

startGatheringOdds();