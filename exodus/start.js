const axios = require('axios');
const fs = require('fs');
const pathModule = require('path');
const { BigQuery } = require('@google-cloud/bigquery');
const path = require('path');
const keyFilename = path.join(__dirname, 'tokyo-carving-401111-4a13a5ef21fa.json');

const WebSocket = require('ws');
const wsUrl = 'wss://dev-feed.top:444/feed?token=hOVUnCHIeWYiXllTOthbfEKqCxxqUriBK2vjdTbEdnshG7dGkrLzsxh2vuyovjUw&set_id=0';
const ws = new WebSocket(wsUrl, {
    rejectUnauthorized: false // Отключение проверки SSL/TLS сертификата
});
const https = require('https');
const httpsAgent = new https.Agent({ rejectUnauthorized: false });

var events;
var id = 100;
//console.log(keyFilename);

const bigquery = new BigQuery({
    keyFilename: keyFilename
});

let globalPageToken; // Инициализация глобального pageToken

async function getRowsFromTable() {
    const datasetId = '610attemptn2';
    const tableId = 'SUPERFEEDRESULT';

    const dataset = bigquery.dataset(datasetId);
    const table = dataset.table(tableId);

    const options = {
        maxResults: 1000, // Получаем по 1000 строк за раз
        pageToken: globalPageToken
    };

    try {
        const [rows, , response] = await table.getRows(options);
        globalPageToken = response.pageToken; // Обновляем глобальный pageToken
        return rows;
    } catch (error) {
        console.error('Error getting rows:', error);
    }
}


async function main() {
    console.log("===========START==========");
    await processRowsAndSendData();
    async function sendToServer(exodusPayload) {
        if (!exodusPayload) {
            return;
        }
        console.log(exodusPayload);
        try {
            const response = await axios.post('http://localhost:3333/bets', exodusPayload);
            return response.data;
        } catch (error) {
            //console.error('Error sending data to server:', error);
            throw error;
        }
    }

    async function processRowsAndSendData() {
        var rows = await getRowsFromTable(id);
        while (true) {
            rows = await getRowsFromTable(id);
            if (rows.length == 0) {
                console.log("END");
                break;
            }
            for (const row of rows) {
                try {
                    // Assuming exodus is a stringified JSON in the row
                    const exodusPayload = JSON.parse(row.exodus);
                    if (exodusPayload === 'undefind') {
                        console.log(row);
                        continue;
                    }
                    if (exodusPayload.bets && exodusPayload.bets.length > 0) {
                        exodusPayload.bets[0].bet_type = row.BK1_bet_type;
                    }
                    // Send payload to server and get response
                    const responseJson = await sendToServer(exodusPayload);

                    // Add server's response JSON to the row object
                    row.result = responseJson.result[0];
                    row.scoreGame = responseJson.result[1].scoreGame;
                    row.resultEvent = responseJson.event_id;
                    console.log(responseJson);
                    await insertAllDataBigQuery(row);
                    // Now you have a row with the new data appended that you can later insert into a new table
                } catch (error) {
                    console.log(row);
                    continue;
                    //console.error('Error processing row:', error);
                }
            }

            id += 100;
        }
    }
}

function savev(msg, path) {
    let data = [];

    if (fs.existsSync(path)) {
        try {
            const fileContent = fs.readFileSync(path, 'utf-8');
            data = JSON.parse(fileContent);
        } catch (error) {
            console.error("Error reading or parsing the file:", error);
        }
    }

    data.push(msg);

    try {
        fs.writeFileSync(path, JSON.stringify(data, null, "\t"));
    } catch (error) {
        console.error("Error writing to the file:", error);
    }
}

async function insertAllDataBigQuery(dataObj) {
    if (dataObj.length === 0) {
        console.error('No data to insert into BigQuery table.');
        return;
    }
    console.log('Insert run');
    const datasetId = '610attemptn2';
    const tableId = 'superexodus';

    const options = {
        datasetId: datasetId,
        tableId: tableId,
        rows: dataObj,
    };

    try {
        await bigquery
            .dataset(datasetId)
            .table(tableId)
            .insert(options.rows);
        console.log(`Inserted ${options.rows.length} rows into BigQuery table.`);
    } catch (error) {
        console.error('Error inserting data:', error);
        if (
            error && error.errors && error.errors.length > 0) {
            error.errors.forEach(err => {
                console.error('Error detail:', err);
                if (err.row) console.error('Error in row:', err.row);
            });
        }
    }
    console.log('Insert done');
}

main();